## UML

[Диаграмма](https://drive.google.com/file/d/1t_BinRZbB1h2uwY2D790LGcsljdkKLyB/view) классов для вопросов(не все классы)

## Questions 
Внизу пример кода, который создает цепочку вопросов. В качестве демонстрации код только для докторов. 

```ruby
start_question = WelcomeQuestion.new #старотовое сообщение, если юзер уже общался, то скипаем

role_question = start_question.next(CityQuestion.new).next(RoleQuestion.new) #вопрос о роли, юзер выбирает doctor или driver. Cкипаем если уже выбрано

#doc_question это когда юзер может выбирать команды, для каждой команды у класса своя цепочка вопросов add_next, del_next, show_next итд
doc_question = role_question.doctor_next(DoctorCommandQuestion.new) #если выбрана роль doctor, то используем цепочку в поле doctor_next

#добавляем цепочку для add_next
doc_place_question = doc_question.add_next(DateQuestion.new).next(TimeQuestion.new).next(PlaceQuestion.new)
#doc_place_question последний вопрос в цепочке add_next
doc_place_question.next(doc_question) # возращаемся обратно к выбору команд

#добавляем цепочку для show_next
doc_show_question = doc_question.show_next(DoctorShowQuestion.new)
doc_show_question.back_next(doc_question) #возращаемся обратно к выбору команд если выбрана кнопка обратно/отмена
match_question = doc_show_question.match_next(MatchQuestion.new) #если доктор выбирает предложение водителя
match_question.next(doc_question) #возращаемся обратно к выбору команд

#добавляем цепочку для del_next
doc_del_question = doc_question.del_next(DoctorDelQuestion.new)
doc_del_question.next(doc_question) #возращаемся обратно к выбору команд
```

## Controller handler

Т.к. возможно будут использоваться другие мессенджеры то в методе Webhooks#telegram должен быть класс которы будет увинирасльным для любых мессенджеров

```ruby
class Core
    def self.handle(**prm)
        u = prm[:user]
        q = find_question(u)# функция возращает объект который соотвествует текущему вопросу
        return q.say(prm)
    end    
end

class WebhooksController
    def telegram
        first_name = params[:message][:from][:first_name]
        telegram_id = params[:message][:from][:id]
        text = params[:message][:text]
        user = User.create_with(name: first_name).find_or_create_by(telegram_id: telegram_id)
        resp = Core::handle(user: user, text: text)
        bot.api.send_message(chat_id: telegram_id, text: resp[:text])
    end
end

```
