require 'test_helper'

class CoreTest < ActiveSupport::TestCase
  def setup
    @core = Core.core
  end

  test "core is present" do
    assert @core.present?
  end

  test "welcome for new user doctor role" do
    u = users(:new_user)
    @core.handle(user: u, text: "/start")

    assert_equal "translation missing: test.questions.welcome", u.send_queue.pop[:text]
    data = u.send_queue.pop
    assert_equal "translation missing: test.questions.city", data[:text]
    assert_equal ["Сумы", "Киев"], data[:keyboard]
    assert_equal true, u.send_queue.empty?
    @core.handle(user:u, text: "no city")
    assert_equal "translation missing: test.questions.error.city", u.send_queue.pop[:text]
    @core.handle(user:u, text: cities(:sumy).name)
    assert_equal u.city.name, cities(:sumy).name
    assert_equal "translation missing: test.questions.role", u.send_queue.pop[:text]
    @core.handle(user:u, text: "no role")
    assert_equal "translation missing: test.questions.error.role", u.send_queue.pop[:text]
    @core.handle(user:u, text: I18n.t('roles.doctor'))
    assert_equal "translation missing: test.questions.doctor_commands", u.send_queue.pop[:text]
  end

  test "doctor commands no phone" do
    u = users :doctor
    assert u.phone.nil?
    Core.new.handle(user: u, text: "hi")
    data = u.send_queue.pop
    assert_equal I18n.t("questions.doctor_commands"), data[:text]
    assert_equal [I18n.t("commands.doctor.add"), I18n.t("commands.doctor.del"), I18n.t("commands.doctor.show")], data[:menu]
    
    Core.new.handle(user: u, text: I18n.t("commands.doctor.add"))
    data = u.send_queue.pop
    assert_equal I18n.t("questions.date"), data[:text]
    assert_equal [I18n.t("date.today"), I18n.t("date.tomorrow"), I18n.t("date.after_tomorrow")], data[:keyboard]

    Core.new.handle(user: u, text: "no date")
    assert_equal I18n.t("questions.error.date"), u.send_queue.pop[:text]

    Core.new.handle(user: u, text: I18n.t("date.tomorrow"))
    assert_equal I18n.t("questions.time"), u.send_queue.pop[:text]

    Core.new.handle(user: u, text: "no time")
    assert_equal I18n.t("questions.error.time"), u.send_queue.pop[:text]

    Core.new.handle(user: u, text: "8:11")
    assert_equal I18n.t("questions.place.start"), u.send_queue.pop[:text]   

    Core.new.handle(user: u, text: "no street")
    assert_equal I18n.t("questions.error.place.name"), u.send_queue.pop[:text]

    Core.new.handle(user: u, text: I18n.t('cities.sumy.place.kurks'))
    assert_equal I18n.t("questions.place.next"), u.send_queue.pop[:text]

    Core.new.handle(user: u, text: I18n.t('cities.sumy.place.kurks'))
    data = u.send_queue.pop
    assert_equal I18n.t("questions.place.next"), data[:text]
    assert_equal I18n.t("questions.place.last"), data[:menu]
    Core.new.handle(user: u, text: I18n.t('cities.sumy.place.kurks'))
    assert_equal I18n.t("questions.place.next"), u.send_queue.pop[:text]
    Core.new.handle(user: u, text: I18n.t("questions.place.last"))
    assert_equal nil, u.context[:doctor]
  end

end