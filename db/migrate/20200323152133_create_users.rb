class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users, id: :uuid do |t|
      t.string :name
      t.integer :telegram_id
      t.references :role

      t.timestamps
    end
    add_index :users, :name
    add_index :users, :telegram_id, unique: true
  end
end
