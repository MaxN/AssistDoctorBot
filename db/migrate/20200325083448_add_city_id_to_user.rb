class AddCityIdToUser < ActiveRecord::Migration[6.0]
  def change
    add_reference :users, :city, type: :uuid, foreign_key: true
  end
end
