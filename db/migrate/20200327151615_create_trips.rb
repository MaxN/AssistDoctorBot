class CreateTrips < ActiveRecord::Migration[6.0]
  def change
    create_table :trips, id: :uuid do |t|
      t.string :kind
      t.references :user, type: :uuid, null: false, foreign_key: true
      t.string :places
      t.date :date
      t.time :time
      t.string :note

      t.timestamps
    end
    add_index :trips, :kind
    add_index :trips, :date
    add_index :trips, :time
  end
end
