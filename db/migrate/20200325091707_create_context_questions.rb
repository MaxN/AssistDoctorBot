class CreateContextQuestions < ActiveRecord::Migration[6.0]
  def change
    create_table :context_questions, id: :uuid do |t|
      t.references :user, type: :uuid, null: false, foreign_key: true
      t.string :data
      t.string :current_question

      t.timestamps
    end
  end
end
