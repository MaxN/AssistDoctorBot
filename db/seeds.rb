# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
doctor_role = Role.find_or_create_by(name: "doctor")
driver_role = Role.find_or_create_by(name: "driver")
sumy = City.find_or_create_by(name: 'Суми')

if Rails.env.development?
  doctor = User.find_or_create_by telegram_id: 123
  doctor.update! name: "dev_doctor", role: doctor_role

  driver = User.find_or_create_by telegram_id: 124
  driver.update! name: "dev_driver", role: driver_role


end