
telegram_token = ENV['ASSIST_DOCTOR_BOT_TOKEN']

namespace :webhooks do
  desc "register telegram webhook"
  task reg_telegram: :environment do
    
    puts "Registring webhooks...."
    cert_fn = File.join(Rails.root,'cert.pem')

    unless File.exist? cert_fn 
      puts "Generating certificate #{cert_fn}"
      #`openssl req -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem -days 3560 -subj "/O=Org/CN=#{ENV['ASSIST_DOCTOR_HOST']}" -nodes`
      `openssl req -newkey rsa:2048 -sha256 -nodes -keyout key.pem -x509 -days 365 -out cert.pem -subj "/C=UA/ST=SUMY/L=SUMY/O=AssistDoctor/CN=#{ENV['ASSIST_DOCTOR_HOST']}"`
    else
      puts "Found certificate #{cert_fn}"
    end

    url = "https://#{ENV['ASSIST_DOCTOR_HOST']}/webhooks/telegram/#{telegram_token}"
    puts `curl -F "url=#{url}" -F "certificate=@#{cert_fn}" https://api.telegram.org/bot#{telegram_token}/setWebhook`
  end

  desc "check webhook info"
  task telegram_webhook_info: :environment do 
  
    puts "Getting Webhook info"
    puts `curl  https://api.telegram.org/bot#{telegram_token}/getWebhookInfo`
  
  end

end
