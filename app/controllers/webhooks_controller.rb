require 'telegram/bot'

class WebhooksController < ActionController::API
  @@semaphore = Mutex.new

  def telegram

    Telegram::Bot::Client.run(ENV['ASSIST_DOCTOR_BOT_TOKEN']) do |bot|
      return if params[:message].nil? && params[:callback_query].nil?
      first_name = ""
      cid = nil
      text = ""
      if params[:message].present?
        first_name = params[:message][:from][:first_name]
        cid = params[:message][:from][:id]
        text = params[:message][:text]
      elsif params[:callback_query].present?
        begin
          first_name = params[:callback_query][:from][:first_name]
          cid = params[:callback_query][:from][:id]
          text = params[:callback_query][:data]
          mid = params[:callback_query][:message][:message_id]
          bot.api.editMessageReplyMarkup(
            chat_id: cid, 
            message_id: mid, 
            reply_markup: Telegram::Bot::Types::InlineKeyboardMarkup.new(
              inline_keyboard: [Telegram::Bot::Types::InlineKeyboardButton.new(text: "\u2714#{text}", callback_data: text)])
          )
        rescue
          return
        end
      end

      u = User.find_by telegram_id: cid
      if text=="/delete" && Rails.env.development?
        bot.api.send_message(chat_id: cid, text: "bye bye", reply_markup: Telegram::Bot::Types::ReplyKeyboardRemove.new(remove_keyboard: true)) 
        u.destroy unless u.nil?
        u = nil
        Core.restart
      end

      @@semaphore.synchronize do
        if u.nil?
          u = User.create! telegram_id: cid, name: first_name
          u.ensure_context_question
        end
      end
      Core.core.handle(user: u, text: text)
      while not u.send_queue.empty?
        data = u.send_queue.pop
        kb = nil

        if data[:keyboard] && data[:keyboard].count > 0
          buttons = data[:keyboard].map do |text|
            Telegram::Bot::Types::InlineKeyboardButton.new(text: text, callback_data: text)
          end
          kb = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: buttons)
        end

        if data[:menu] && data[:menu].count > 0
          buttons = data[:menu].map do |text|
            Telegram::Bot::Types::KeyboardButton.new(text: text)
          end
          kb = Telegram::Bot::Types::ReplyKeyboardMarkup.new(keyboard: buttons)  
        end
        
        bot.api.send_message(chat_id: cid, text: data[:text], reply_markup: kb)        
      end

      # bot.api.send_message(chat_id: cid, text: "Привіт, #{first_name}")
      
    end
  ensure
    render json: {}
  end

end
