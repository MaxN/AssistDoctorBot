class Core
  
  def initialize
    @send_message = []
    @start_question = Questions::Welcome.new
    role_question = @start_question.next(Questions::City.new).next(Questions::Role.new) 
    doc_commands = role_question.doctor_next(Questions::DoctorCommands.new)
    doc_commands.add_next(Questions::Date.new).next(Questions::Time.new).next(Questions::Place.new).next(Questions::CreateTrip.new('ask')).next(doc_commands)

    driver_commands = role_question.driver_next(Questions::DriverCommands.new)
  end
  
  def handle(**prm)
    u = prm[:user]
    q = find_question(u)
    u.current_question = q
    continue = true
    while continue do
      continue = u.current_question.say(prm)
      prm[:text] = nil
      raise 'please return continue' if continue.nil?
    end
  end

  def find_question(u)
  
    return u.current_question unless u.current_question.nil?
    name = u.context_question.current_question
    @start_question.find_question(name, []) || @start_question 
  end

  @@core = Core.new
  
  def self.core
    @@core 
  end

  def self.restart
    @@core = Core.new
  end
end