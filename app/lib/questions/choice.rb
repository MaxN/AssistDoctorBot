class Questions::Choice < Questions::Base
  def choices  
  end
  def name
  end

  def say(**prm)
    u = prm[:user]
    text = prm[:text]
    if text.nil?
      u.send_queue << {text: I18n.t("questions.#{name}"), keyboard: choices}
      return false
    else

      valid = choices.include? text
      if valid
        u.current_question = @next_question
        return true
      else
        u.send_queue << {text: I18n.t("questions.error.#{name}")}
        return false
      end
    end
  end
end
