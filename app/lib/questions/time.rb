class Questions::Time < Questions::Input
  def validate_expr  
    /^([0-1]?[0-9]|[2][0-3]):([0-5][0-9])$$/i
  end

  def name
    'time'
  end
  def say(**prm)
    res = super
    if res
      u = prm[:user]
      u.context[:time] = prm[:text]
    end
    res
  end

end