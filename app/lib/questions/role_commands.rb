class Questions::RoleCommands < Questions::Base
  def add_next(question)
    @add_next_question = question
    question
  end
  def del_next(question)
    @del_next_question = question
    question
  end
  
  def show_next(question)
    @show_next_question = question
    question
  end
  def role
  end
  
  def all_next
    [@add_next_question, @del_next_question, @show_next_question]
  end
  
  def send_menu(user)
    user.send_queue << {text: I18n.t("questions.#{role}_commands"), 
        menu: [
          I18n.t("commands.#{role}.add"),
          I18n.t("commands.#{role}.del"),
          I18n.t("commands.#{role}.show"),
        ]}
  end
  def say(**prm)
    u = prm[:user]
    text = prm[:text]
    if text.nil?
      send_menu(u)
      return false
    else
      case text
        when I18n.t("commands.#{role}.add")
          u.current_question = @add_next_question
          return true
        when I18n.t("commands.#{role}.del")
          u.current_question = @del_next_question
          return true
        when I18n.t("commands.#{role}.show")
          u.current_question = @show_next_question
          return true          
      end
      send_menu(u)
      return false
    end
  end  
end