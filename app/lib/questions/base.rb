class Questions::Base
  def initialize
    @next_question = nil
  end

  def next(question)
    @next_question = question
    question
  end
  
  def say(**prm)
    raise 'unimplemented'
  end

  def read(**prm)
    raise 'unimplemented'
  end
  
  def all_next
    [@next_question]
  end

  def find_question(name, question_list)
    all_next.each do |q|
      next if q.nil?
      #byebug if self.class.name == "Questions::CreateTrip"
      puts "#{self.class.name}: #{q.class.name} == #{name}"
      return q if q.class.name == name
      if question_list.exclude?(q)
        question_list<<q
        q = q.find_question(name, question_list)
        return q unless q.nil?
      end
    end
    nil
  end
end