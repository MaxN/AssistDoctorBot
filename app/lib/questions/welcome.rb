class Questions::Welcome < Questions::Base
  def say(**prm)
    u = prm[:user]
    u.current_question = @next_question
    if u.role.present? # check if user alread has assigned role
      return true
    else
      u.send_queue << {text: I18n.t('questions.welcome', name: u.name)}
      return true
    end
  end
end
