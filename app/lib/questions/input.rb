class Questions::Input < Questions::Base
  def validate_expr  
  end
  
  def name
  end

  def say(**prm)
    u = prm[:user]
    text = prm[:text]
    if text.nil?
      u.send_queue << {text: I18n.t("questions.#{name}")}
      return false
    else

      valid = validate_expr =~ text
      if valid
        u.current_question = @next_question
        return true
      else
        u.send_queue << {text: I18n.t("questions.error.#{name}")}
        return false
      end

    end
  end

end