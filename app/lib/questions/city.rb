class Questions::City < Questions::Base
  def say(**prm)
    u = prm[:user]
   
    if u.city.present? # check if user alread has assigned role
      u.current_question = @next_question
      return true
    else
      if prm[:text].nil?
        u.send_queue<<{text: I18n.t('questions.city'), keyboard: City.all.map(&:name)}
        return false
      else
        #validate
        valid = false
        City.all.each do |city|
          valid ||= city.name == prm[:text]
        end
        
        if valid
          u.current_question = @next_question
          u.update city: City.find_by(name: prm[:text])
          return true
        else
          u.send_queue<<{text: I18n.t('questions.error.city'), keyboard: City.all.map(&:name)}
          return false
        end
      end
    end
  end
end
