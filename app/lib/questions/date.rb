class Questions::Date < Questions::Choice
  def choices
    [I18n.t("date.today"), I18n.t("date.tomorrow"), I18n.t("date.after_tomorrow") ]
  end

  def name
    'date'
  end
  
  def say(**prm)
    res = super
    if res
      u = prm[:user]
      u.context[:date] = prm[:text]
    end
    res
  end
end
