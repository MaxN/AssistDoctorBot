class Questions::Place < Questions::Base
  
  def places
    [I18n.t('cities.sumy.place.kurks')]
  end
  def ask_place(user)
  end

  def say(**prm)
    u = prm[:user]
    text = prm[:text]
    
    if text.nil?
      u.send_queue << {text: I18n.t('questions.place.start'), keyboard: places} if (u.context[:places] || []).count == 0
    else
      valid = false
      places.each do |place|
        valid ||= place == text
      end
      if valid
        places = u.context[:places] || []
        places << text
        u.context[:places] = places
        u.send_queue << {text: I18n.t("questions.place.next"), menu: I18n.t("questions.place.last")}
      elsif I18n.t("questions.place.last") == text
        return true

      else
        u.send_queue << {text: I18n.t('questions.error.place.name')}
      end
    end
    return false

  end
end