class Questions::Role < Questions::Base
  def doctor_next(question)
    @doctor_next_question = question
    question
  end

  
  def driver_next(question)
    @driver_next_question = question
    question
  end

  def all_next
    [@doctor_next_question, @driver_next_question]
  end
  def set_next_question(user)
    if user.role.name == "doctor"
      user.current_question = @doctor_next_question
      true
    elsif user.role.name == "driver"
      user.current_question = @driver_next_question
      true
    else
      false
    end
  end

  def say(**prm)
    u = prm[:user]
    if u.role.present? # check if user alread has assigned role
      unless set_next_question(u)
        u.send_queue << {text: I18n.t('questions.role'), keyboard: [I18n.t('roles.doctor'), I18n.t('roles.driver')]}
        return false
      end

      return true
    else
      if prm[:text].nil?
        u.send_queue << {text: I18n.t('questions.role'), keyboard: [I18n.t('roles.doctor'), I18n.t('roles.driver')]}
        return false
      else
        valid = I18n.t('roles.doctor') == prm[:text] || I18n.t('roles.driver') == prm[:text]
        if valid
          u.update role: Role.find_or_create_by(name: 'doctor') if I18n.t('roles.doctor') == prm[:text]
          u.update role: Role.find_or_create_by(name: 'driver') if I18n.t('roles.driver') == prm[:text]
          
          return set_next_question(u)
        else
          u.send_queue <<{text: I18n.t('questions.error.role'), keyboard: [I18n.t('roles.doctor'), I18n.t('roles.driver')]}
          return false
        end
      end
    end
  end
end
