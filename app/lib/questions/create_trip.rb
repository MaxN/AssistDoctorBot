class Questions::CreateTrip < Questions::Base
  def initialize(trip_kind)
    
    @kind = trip_kind
  end

  def say(**prm)
    u = prm[:user]
    
    u.create_trip! kind: @kind, places: u.context[:places], date: u.context[:date], time: u.context[:time], note: u.context[:note]
    u.context.clear
    return true

  end
end