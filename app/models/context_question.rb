class ContextQuestion < ApplicationRecord
  belongs_to :user
  
  def [](key)
    JSON.parse(data || "{}")[key]
  end

  def []=(key, value)
    tmp = JSON.parse(data || "{}")
    tmp[key] = value
    update! data: JSON.generate(tmp)
  end

  def clear
    update! data: "{}"
  end

end
