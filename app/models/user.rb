class User < ApplicationRecord
  belongs_to :role, optional: true
  belongs_to :city, optional: true
  has_one :context_question, dependent: :destroy
  has_many :trips, dependent: :destroy

  attr_accessor :send_queue

  after_initialize do |user|
    @send_queue = Queue.new
  end
  def context
    context_question
  end
  
  def lang
    'ru'
  end

  def ensure_context_question
    raise 'not saved' unless self.id.present?
    self.context_question = ContextQuestion.find_or_create_by id: self.id
  end
  
  def current_question
    @current_question
  end

  def current_question=(q)
    @current_question = q
    self.context_question.update! current_question: q.class.name unless q.nil?
  end

end
